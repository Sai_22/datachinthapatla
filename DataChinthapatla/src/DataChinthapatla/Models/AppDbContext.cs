﻿using Microsoft.Data.Entity;


namespace DataChinthapatla.Models
{

    public class AppDbContext : DbContext
    {

        public DbSet<Location> Locations { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }

    }
}






