﻿using System;
using System.Linq;
using System.IO;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
namespace DataChinthapatla.Models
{
    public static class AppSeedData
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        public static void Initialize(IServiceProvider serviceProvider)
        {

            var context = serviceProvider.GetService<AppDbContext>();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;   // DB already seeded
            }
            if(context.Vehicles.Any())
            {
                return;
            }
           // var l1 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
            //var l2 = context.Locations.Add(new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" });
           // var l3 = context.Locations.Add(new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" });
           // var l4 = context.Locations.Add(new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" });
           // var l5 = context.Locations.Add(new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" });
           // var l6 = context.Locations.Add(new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" });
           // var l7 = context.Locations.Add(new Location() { Latitude = 41.8339042, Longitude = 88.0123526, Place = "Chicago", Country = "USA" });

           // context.Vehicles.AddRange(
             //   new Vehicle() { VehicleId = "F123", Number = 1, VehicleModel = "v1", LocationID = l1.Entity.LocationID },
              //  new Vehicle() { VehicleId = "F124", Number = 2, VehicleModel = "v2", LocationID = l2.Entity.LocationID },
              //  new Vehicle() { VehicleId = "F125", Number = 3, VehicleModel = "v3" , LocationID= l3.Entity.LocationID},
              //  new Vehicle() { VehicleId = "F126", Number = 4, VehicleModel = "v4" , LocationID= l4.Entity.LocationID },
              //  new Vehicle() { VehicleId = "F127", Number = 5, VehicleModel = "v5" , LocationID= l5.Entity.LocationID},
               // new Vehicle() { VehicleId = "F128", Number = 6, VehicleModel = "v6" , LocationID= l6.Entity.LocationID},
               // new Vehicle() { VehicleId = "F129", Number = 7, VehicleModel = "v7" ,LocationID= l7.Entity.LocationID }



              //  );
          //  context.SaveChanges();
        }
        private static void SeedMoviesFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "vehicle.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Vehicle.ReadAllFromCSV(source);
            List<Vehicle> lst = Vehicle.ReadAllFromCSV(source);
            context.Vehicles.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }
    }
}







