﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

using System.IO;


namespace DataChinthapatla.Models
{
    public class Vehicle
    {
        [ScaffoldColumn(false)]
        [Display(Name = "Vehicle Available")]
        [Key]
        public int VehicleId { get; set; }

        [Display(Name = "Number")]
        public int VNumber { get; set; }

        [Display(Name = "VehicleModel")]
        public string VehicleModel { get; set; }

        public int LocationID { get; set; }

        public virtual Location Location { get; set; }

        public static List<Vehicle> ReadAllFromCSV(string filepath)
        {
            List<Vehicle> lst = File.ReadAllLines(filepath)
                                         .Skip(1)
                                         .Select(v => Vehicle.OneFromCsv(v))
                                         .ToList();
            return lst;
        }

        public static Vehicle OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Vehicle item = new Vehicle();

            int i = 0;
            item.VNumber = Convert.ToInt32(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);
            item.VehicleModel = Convert.ToString(values[i++]);
            item.VehicleId = Convert.ToInt32(values[i++]);

            return item;
        }
    }
}
